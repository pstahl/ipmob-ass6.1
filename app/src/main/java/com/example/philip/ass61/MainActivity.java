package com.example.philip.ass61;

import android.app.Activity;
import android.app.ListActivity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Date;


public class MainActivity extends ListActivity {

    private String[] CALL_FIELDS = {
            CallLog.Calls.NUMBER,
            CallLog.Calls.TYPE,
            CallLog.Calls.DATE,
            CallLog.Calls.DURATION
    };

    private String[] CALL_PROJECTION = {
            CallLog.Calls._ID,
            CallLog.Calls.NUMBER,
            CallLog.Calls.TYPE,
            CallLog.Calls.DATE,
            CallLog.Calls.DURATION
    };

    private final static int[] TO_IDS = {
            R.id.textView_callnumber,
            R.id.textView_calltype,
            R.id.textView_calldate,
            R.id.textView_callduration
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);

        Cursor query = this.managedQuery(
                CallLog.Calls.CONTENT_URI, CALL_PROJECTION, null, null, null);

        final ListAdapter adapter = new SimpleCursorAdapter(
                this, R.layout.list_row, query,
                CALL_FIELDS,
                TO_IDS);

        TextView text = (TextView) findViewById(R.id.test_view);
        ListView listView = (ListView) findViewById(android.R.id.list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor item = (Cursor) adapter.getItem(position);
                String uri = item.getString(item.getColumnIndex(CallLog.Calls.NUMBER));
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + uri));
                startActivity(intent);
            }
        });

        this.setListAdapter(adapter);
    }
}
